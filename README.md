# Dewanonton HTML Version

To start develop, you have to
 * Install [NodeJS](https://nodejs.org/en) or [IOJS](https://iojs.org/en)
 * Install `gulp`:
	* `npm install gulp -g`  ,if error EACCES `sudo npm install gulp -g`
 * Install dependencies:
	* `npm install`
 * Build destination files `gulp build`
 * Watch files `gulp serve`
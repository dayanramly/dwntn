var gulp        	= require('gulp'),
	browserSync 	= require('browser-sync').create(),
	sass        	= require('gulp-sass'),
	autoprefixer	= require('gulp-autoprefixer'),
	uglify			= require('gulp-uglify'),
	rename			= require('gulp-rename'),
	clean 			= require('gulp-clean'),
	cleanCss 		= require('gulp-clean-css'),
	sourcemaps 		= require('gulp-sourcemaps'),
	runSequence 	= require('run-sequence');


//ionicons
gulp.task('ioniconsFont', function(){
    return gulp.src('node_modules/ionicons/dist/fonts/*')
        .pipe(gulp.dest("dist/fonts"));
})

gulp.task('ioniconsCss', function(){
    return gulp.src('node_modules/ionicons/dist/scss/ionicons.scss')
        .pipe(sass())
        .pipe(gulp.dest("dist/css/"));
})

//flaticon
gulp.task('flaticonFont', function(){
    return gulp.src('src/font/flaticon/*')
    	.pipe(gulp.dest("dist/fonts/flaticon"));
})

gulp.task('flaticonCss', function(){
	return gulp.src(['src/scss/vendor/flaticon/flaticon.scss', 'src/scss/vendor/flaticon/*.scss'])
		.pipe(sass())
		.pipe(gulp.dest("dist/css/"));
})

//proximaFont
gulp.task('proximaFont', function(){
    return gulp.src('src/font/ProximaNova/*')
        .pipe(gulp.dest("dist/fonts/ProximaNova"));
})

//image
gulp.task('image', function(){
    return gulp.src(['src/img/*.png', 'src/img/*.jpg', 'src/img/*.mp4'])
        .pipe(gulp.dest("dist/img/"));
})


// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'])
        .pipe(sass())
        .pipe(gulp.dest("dist/css"))
        .pipe(browserSync.stream());
});

//compile style task
gulp.task('mainSass', function() {
	return gulp.src(['src/scss/style.scss', 'src/scss/*.scss'])
		.pipe(sass())
		.pipe(gulp.dest("dist/css"));
})

// Move the javascript files into our /src/js folder
gulp.task('js', function() {
    return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js', 'node_modules/jquery/dist/jquery.min.js', 'node_modules/popper.js/dist/popper.js'])
        .pipe(gulp.dest("dist/js"))
        .pipe(browserSync.stream());
});

// Static Server + watching scss/html files
gulp.task('serve', ['sass', 'mainSass'], function() {

    browserSync.init({
        server: "./"  
    });

    gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/style.scss', 'src/scss/pages/*.scss', 'src/scss/partials/*.scss', 'src/scss/*.scss'], ['sass']);
    gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('default', ['js','serve']);

//clean dist folder
gulp.task('clean', function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});

gulp.task('build', ['clean'], function () {
  runSequence(
    'proximaFont',
    'ioniconsFont',
    'ioniconsCss',
    'flaticonFont',
    'flaticonCss',
    'image',
    'sass',
    'mainSass',
    'js'
  );
});